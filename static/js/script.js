$(document).ready(function () {
    $('#submit-log').on('click', function (e) {
        let form = $('#add-log-form');
        let data = form.serialize();
        // console.log(data);
        // form.submit();
        $.ajax({
            url: form.attr('action'),
            data,
            method: 'post',
            success: function (d) {
                console.log(d.html);
                $('table.time-log > tbody').append(d.html);
                $('#exampleModal').modal('hide')
            }
        }).fail(function (e) {
            let d = e.responseJSON;
            if(d.errors) {
                for(e in d.errors) {
                    let errorForm = $('#log-form-errors');
                    errorForm.append(`<p>${e}: ${d.errors[e]}</p>`);
                    errorForm.removeClass('d-none');
                }
            }
        });
    });

    $('.remove-log').on('click', function (e) {
       e.preventDefault();
       let modal = $('#exampleModal');
       let removeURL = $(this).attr('href');
       let button = $('#remove-log-modal');
       console.log(removeURL);
       button.data('remove-url', removeURL);
       button.data('id', $(this).closest('tr').data('id'));
       modal.modal('show');
    });
    $('#remove-log-modal').on('click', function () {
        let url = $(this).data('remove-url');
        let id = $(this).data('id');
        let data = {'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()};
        $.ajax({
            url: url,
            data,
            method: 'POST',
            success: function () {
                $(`tr[data-id=${id}]`).remove();
                $('#exampleModal').modal('hide')
            }
        })
    });
    $( ".dateinput" ).datepicker({dateFormat: 'yy-mm-dd', showButtonPanel: true});
    $( ".datetimeinput" ).datepicker({dateFormat: 'yy-mm-dd 00:00', showButtonPanel: true});
});
