from django.core.exceptions import ValidationError

from core.forms import WatchModelForm
from .models import TimeLog


class TimeLogForm(WatchModelForm):
    class Meta:
        model = TimeLog
        fields = ('time', 'date', 'comment')

    watch_fields = ('time', 'date', 'comment')

    def clean_time(self):
        if int(self.cleaned_data['time']) <= 0:
            raise ValidationError('Time must be greater then 0')
        return self.cleaned_data['time']

    def get_subscribers(self):
        return self.instance.task.get_watchers()
