from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView

from core.mixins import StaffRequiredMixin
from tasks.models import Task
from .forms import TimeLogForm
from .models import TimeLog


class LogTimeView(LoginRequiredMixin, CreateView):
    model = TimeLog
    form_class = TimeLogForm

    def dispatch(self, request, *args, **kwargs):
        returned = super().dispatch(request, *args, **kwargs)
        if self.object and self.object.task.assigned_to != request.user and not request.user.is_staff:
            return self.handle_no_permission()
        return returned

    def get_success_url(self):
        return reverse_lazy('task_detail', kwargs={'slug': self.object.task.slug})

    def form_valid(self, form):
        form.instance.task = get_object_or_404(Task, pk=self.kwargs['task_pk'])
        form.instance.author = self.request.user
        self.object = form.save(request=self.request)
        if self.request.is_ajax():
            comment = render_to_string('tasks/parts/log.html', context={'log': self.object})
            return JsonResponse({'html': comment, 'id': self.object.pk})
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        if self.request.is_ajax:
            return JsonResponse({'errors': form.errors}, status=400)
        return super().form_invalid(form)


class TimeLogView(LoginRequiredMixin, ListView):
    template_name = 'tasks/timelog.html'
    paginate_by = 15

    def get_queryset(self):
        if self.request.user.is_staff:
            return TimeLog.objects.all()
        else:
            return TimeLog.objects.filter(task__assigned_to=self.request.user)


class DeleteLogView(StaffRequiredMixin, DeleteView):
    model = TimeLog
    success_url = reverse_lazy('timelog')