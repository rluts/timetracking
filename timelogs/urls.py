from django.urls import path
from .views import TimeLogView, DeleteLogView, LogTimeView


urlpatterns = [
    path('timelog/', TimeLogView.as_view(), name='timelog'),
    path('timelog/<int:pk>/', DeleteLogView.as_view(), name='timelog_delete'),
    path('timelog/create/<int:task_pk>/', LogTimeView.as_view(), name='log_time'),
]
