from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone

from tasks.models import Task


User = get_user_model()


class TimeLog(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    time = models.PositiveIntegerField(default=0)
    date = models.DateField(default=timezone.now)
    created_date = models.DateTimeField(auto_now_add=True)
    comment = models.CharField(max_length=255)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='time_logs')

    def __str__(self):
        return "{} ({}hs.)".format(self.author, self.time)
