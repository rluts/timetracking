#!/bin/sh

echo "Waiting for mysql..."
while ! nc -z mysql 3306; do
  sleep 2
  echo "mysql doesn't started yet. sleeping..."
done
echo "mysql started"