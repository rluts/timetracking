from django.urls import path
from .views import CreateProjectView, ProjectListView, ProjectDetailView, UpdateProjectView

urlpatterns = [
    path('projects/create/', CreateProjectView.as_view(), name='create_project'),
    path('projects/', ProjectListView.as_view(), name='project_list'),
    path('projects/<slug:slug>/', ProjectDetailView.as_view(), name='project_detail'),
    path('projects/edit/<slug:slug>/', UpdateProjectView.as_view(), name='update_project'),
]
