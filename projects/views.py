from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView

from core.mixins import StaffRequiredMixin
from tasks.models import Task
from .forms import ProjectForm
from .models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    template_name = 'tasks/project_list.html'
    paginate_by = 15

    def get_queryset(self):
        if self.request.user.is_staff:
            return Project.objects.all()
        else:
            return Project.objects.filter(tasks__assigned_to=self.request.user).distinct()


class CreateProjectView(StaffRequiredMixin, CreateView):
    model = Project
    form_class = ProjectForm
    success_url = reverse_lazy('project_list')
    template_name = 'tasks/form_page.html'


class UpdateProjectView(StaffRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    template_name = 'tasks/form_page.html'

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'slug': self.object.slug})


class ProjectDetailView(LoginRequiredMixin, DetailView):
    template_name = 'tasks/project_page.html'
    model = Project

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_staff:
            context['tasks'] = Task.objects.filter(project=self.object).order_by('-id')
        else:
            context['tasks'] = Task.objects.filter(assigned_to=self.request.user, project=self.object).order_by('-id')
        return context
