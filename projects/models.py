from django.db import models
from core.models import AbstractRichTextModel
from django.urls import reverse_lazy


class Project(AbstractRichTextModel):

    def get_absolute_url(self):
        return reverse_lazy('project_detail', kwargs={'slug': self.slug})
