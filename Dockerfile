FROM python:3.6

RUN apt-get -y update \
  && apt-get install -y gettext \
  && apt-get install -y default-libmysqlclient-dev \
  && apt-get install -y netcat \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/

WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8000