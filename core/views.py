from avatar.forms import UploadAvatarForm
from avatar.views import add
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from .forms import UserForm, UpdateUserForm
from .mixins import StaffRequiredMixin

User = get_user_model()


def home(request):
    return HttpResponseRedirect(reverse_lazy('project_list'))


class UserListView(LoginRequiredMixin, ListView):
    template_name = 'tasks/users.html'
    model = User
    paginate_by = 15


class AddUserView(StaffRequiredMixin, CreateView):
    template_name = 'tasks/form_page.html'
    model = User
    form_class = UserForm
    success_url = reverse_lazy('user_list')


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'tasks/profile.html'
    form_class = UpdateUserForm
    model = User
    success_url = reverse_lazy('user_list')

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if not self.request.user.is_staff:
            del form.fields['is_staff']
            del form.fields['is_active']
        return form

    def get_object(self, queryset=None):
        if self.request.user.is_staff and self.kwargs.get('user'):
            return get_object_or_404(User, pk=self.kwargs['user'])
        else:
            return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user == self.object:
            context['avatar_form'] = UploadAvatarForm(user=self.object)
        return context


def avatar_add(request, **kwargs):
    return add(request, next_override=reverse_lazy('profile'), **kwargs)