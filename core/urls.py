from django.urls import path
from .views import UserListView, AddUserView, ProfileView, avatar_add
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('users/', UserListView.as_view(), name='user_list'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('users/add/', AddUserView.as_view(), name='add_user'),
    path('users/profile/', ProfileView.as_view(), name='profile'),
    path('users/profile/<int:user>/', ProfileView.as_view(), name='other_profile'),
    path('users/profile/avatar/add/', avatar_add, name='avatar_add'),
]
