from ckeditor.fields import RichTextField
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse_lazy
from django.utils.text import slugify

from .utils import html_clean


class User(AbstractUser):
    email = models.EmailField(unique=True)
    position = models.CharField(max_length=255, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)

    def get_full_name(self):
        return super().get_full_name() or self.username

    def get_absolute_url(self):
        return reverse_lazy('other_profile', kwargs={'pk': self.pk})


class AbstractRichTextModel(models.Model):
    class Meta:
        abstract = True

    slug = models.SlugField(null=True, blank=True, unique=True)
    name = models.CharField(max_length=255)
    description = RichTextField()
    created_date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.description = html_clean(self.description)
        super(AbstractRichTextModel, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = "{}-{}".format(slugify(self.name), self.pk)
            self.save()

    def __str__(self):
        return self.name
