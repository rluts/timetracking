import string
import random
from django import forms
from django.contrib.auth import get_user_model
from .tasks import send_celery_mail

User = get_user_model()


class WatchModelForm(forms.ModelForm):
    """
    Abstract form with notification about changes
    """
    watch_fields = None
    changed_message = "{} #{} has been changed"
    added_message = "New {} added"

    def get_watch_fields(self):
        if self.watch_fields is None:
            raise NotImplementedError
        return self.watch_fields

    def get_subscribers(self):
        raise NotImplementedError

    def get_link(self, request):
        if getattr(self.instance, 'get_absolute_url', None):
            location = str(self.instance.get_absolute_url())
            url = request.build_absolute_uri(location)
            link = '<a href="{}">Read more</a>'.format(url)
            return link
        else:
            return ''

    def save(self, commit=True, request=None):
        previous_instance = self._meta.model.objects.get(pk=self.instance.pk) if self.instance.pk else None
        super(WatchModelForm, self).save(commit)
        if commit:
            message = self.changed_message if previous_instance else self.added_message
            subject = message.format(self.instance._meta.verbose_name.title(), self.instance.pk)
            message = "{} by {}<br><br>".format(subject, request.user.get_full_name())
            changed = False
            if previous_instance:
                for field in self.get_watch_fields():
                    if getattr(self.instance, field) != getattr(previous_instance, field):
                        changed = True
                        message += '<b>Old {}</b>: {}<br>\n'.format(previous_instance._meta.get_field(field).verbose_name.title(),
                                                               getattr(previous_instance, field) or '-')
                        message += '<b>New {}</b>: {}<br><br>'.format(self.instance._meta.get_field(field).verbose_name.title(),
                                                               getattr(self.instance, field) or '-')
            else:
                changed = True
                for field in self.get_watch_fields():
                    message += '{}: {}<br><br>'.format(self.instance._meta.get_field(field).verbose_name.title(),
                                                       getattr(self.instance, field) or '-')
            if changed:
                message += "<br><br>{}".format(self.get_link(request))
                send_celery_mail.delay(subject, message, self.get_subscribers(), html_message=message)
        return self.instance


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('permissions', 'is_superuser', 'groups', 'user_permissions', 'password', 'last_login', 'date_joined')

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    @staticmethod
    def _generate_password():
        letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
        return ''.join(random.choice(letters) for _ in range(12))

    def save(self, commit=True):
        password = self._generate_password()
        instance = super().save(commit)
        instance.set_password(password)
        instance.save()
        message = 'You user has created on the Timetracker site.\n<br>' \
                  'Your username: {}\n<br>' \
                  'Your password: {}'.format(self.instance.username, password)
        send_celery_mail.delay('Your user has been created', message, [self.instance.email], html_message=message)
        return instance


class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ('permissions', 'is_superuser', 'groups', 'user_permissions', 'password', 'last_login', 'date_joined')

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)