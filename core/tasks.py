from django.core.mail import send_mail
from django.conf import settings
from celery import shared_task


@shared_task
def send_celery_mail(subject, message, recipient_list,
                     fail_silently=False, auth_user=None, auth_password=None,
                     connection=None, html_message=None):
    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, recipient_list, fail_silently, auth_user, auth_password,
              connection, html_message)
