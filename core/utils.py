import bleach
from django.conf import settings


def html_clean(text):
    return bleach.clean(text, tags=settings.WYSIWYG_ALLOWED_TAGS)
