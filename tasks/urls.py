from django.urls import path

from .views import CreateTaskView, TaskListView, UpdateTaskView, TaskDetailView, AddCommentView


urlpatterns = [
    path('tasks/', TaskListView.as_view(), name='task_list'),
    path('tasks/<slug:slug>/', TaskDetailView.as_view(), name='task_detail'),
    path('tasks/<int:task_pk>/comments/create/', AddCommentView.as_view(), name='add_comment'),
    path('tasks/edit/<slug:slug>/', UpdateTaskView.as_view(), name='edit_task'),
    path('tasks/<int:project_pk>/create/', CreateTaskView.as_view(), name='create_task'),
]
