from ckeditor.fields import RichTextField
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse_lazy
from django.utils import timezone

from core.models import AbstractRichTextModel
from core.utils import html_clean
from projects.models import Project


User = get_user_model()


class Task(AbstractRichTextModel):
    TYPE_CHOICES = (
        ('feature', 'Feature'),
        ('bug', 'Bug')
    )
    PRIORITY_CHOICES = (
        ('normal', 'Normal'),
        ('high', 'High'),
        ('urgent', 'Urgent')
    )
    STATUS_CHOICES = (
        ('new', 'New'),
        ('in_progress', 'In progress'),
        ('solved', 'Solved'),
        ('declined', 'Declined')
    )
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    type = models.CharField(max_length=8, choices=TYPE_CHOICES)
    priority = models.CharField(max_length=8, choices=PRIORITY_CHOICES)
    estimate = models.PositiveIntegerField(default=0)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks')
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True,
                                related_name='created_tasks')
    assigned_to = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='assigned_tasks',
                                    null=True, blank=True)
    status = models.CharField(max_length=12, choices=STATUS_CHOICES, default='new')
    closed = models.BooleanField(default=False, db_index=True)

    def get_absolute_url(self):
        return reverse_lazy('task_detail', kwargs={'slug': self.slug})

    def get_watchers(self):
        watchers = []
        if self.creator and self.creator.email:
            watchers.append(self.creator.email)
        if self.assigned_to and self.assigned_to.email:
            watchers.append(self.assigned_to.email)
        return list(set(watchers))

    def save(self, *args, **kwargs):
        if not self.start_date and self.status == 'in_progress':
            self.start_date = timezone.now()
        super(Task, self).save(*args, **kwargs)


class TaskComment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = RichTextField()
    comment_date = models.DateTimeField(auto_now_add=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='comments')

    def get_absolute_url(self):
        return self.task.get_absolute_url()

    def save(self, *args, **kwargs):
        self.comment = html_clean(self.comment)
        return super().save(*args, **kwargs)

    def __str__(self):
        return "%s %s" % (self.author.get_full_name(), self.comment_date.strftime("%Y-%m-%d"))
