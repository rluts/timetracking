from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.http import Http404
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView

from core.mixins import StaffRequiredMixin
from timelogs.forms import TimeLogForm
from timelogs.models import TimeLog
from .forms import TaskForm, TaskCommentForm
from .models import Project, Task, TaskComment

User = get_user_model()


class CreateTaskView(StaffRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm
    template_name = 'tasks/form_page.html'

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'slug': self.object.project.slug})

    def form_valid(self, form):
        form.instance.project = get_object_or_404(Project, pk=self.kwargs['project_pk'])
        form.instance.author = self.request.user
        self.object = form.save(request=self.request)
        return HttpResponseRedirect(self.get_success_url())


class UpdateTaskView(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskForm
    template_name = 'tasks/form_page.html'

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'slug': self.object.project.slug})

    def form_valid(self, form):
        self.object = form.save(request=self.request)
        return HttpResponseRedirect(self.get_success_url())


class TaskListView(LoginRequiredMixin, ListView):
    template_name = 'tasks/task_list.html'
    paginate_by = 15

    def get_queryset(self):
        if self.request.user.is_staff:
            return Task.objects.all()
        else:
            return Task.objects.filter(assigned_to=self.request.user).order_by('-id')


class TaskDetailView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = 'tasks/task_page.html'

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if not self.request.user.is_staff and obj.assigned_to != self.request.user:
            raise Http404
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = TaskCommentForm
        context['log_form'] = TimeLogForm
        context['spent_time'] = TimeLog.objects.filter(task=self.object).aggregate(sum=Sum('time'))['sum'] or 0
        return context


class AddCommentView(LoginRequiredMixin, CreateView):
    form_class = TaskCommentForm
    template_name = 'tasks/form_page.html'
    model = TaskComment

    def get_success_url(self):
        return reverse_lazy('task_detail', kwargs={'slug': self.object.task.slug})

    def form_valid(self, form):
        form.instance.task = get_object_or_404(Task, pk=self.kwargs['task_pk'])
        form.instance.author = self.request.user
        self.object = form.save(request=self.request)
        if self.request.is_ajax():
            comment = render_to_string('tasks/parts/comment.html', context={'comment': self.object})
            return JsonResponse({'html': comment, 'id': self.object.pk})
        return HttpResponseRedirect(self.get_success_url())
