from django.conf import settings
from django.contrib import admin

from .models import Project, Task, User

# Register your models here.

if settings.DEBUG:
    admin.site.register((Project, Task, User))
