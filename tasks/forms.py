from django import forms

from core.forms import WatchModelForm
from .models import Project, Task, TaskComment


class TaskForm(WatchModelForm):
    class Meta:
        model = Task
        exclude = ('closed', 'creator', 'project')

    watch_fields = ('name', 'description', 'status',
                    'start_date', 'end_date', 'type', 'priority', 'estimate', 'creator', 'assigned_to', 'creator')

    def save(self, commit=True, request=None):
        if not self.instance.pk:
            self.instance.creator = request.user
        return super().save(commit, request)

    def get_subscribers(self):
        return self.instance.get_watchers()


class TaskCommentForm(WatchModelForm):
    class Meta:
        model = TaskComment
        fields = ('comment', )
    watch_fields = ('comment', )

    def get_subscribers(self):
        return self.instance.task.get_watchers()



