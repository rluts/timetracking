# Generated by Django 2.2.3 on 2019-08-05 11:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_auto_20190803_1819'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='timelog',
            name='author',
        ),
        migrations.RemoveField(
            model_name='timelog',
            name='task',
        ),
        migrations.AlterField(
            model_name='task',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to='projects.Project'),
        ),
        migrations.DeleteModel(
            name='Project',
        ),
        migrations.DeleteModel(
            name='TimeLog',
        ),
    ]
